<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Merge\ScoreCalculationDefinition;
use PhpExtended\Score\ScoreCollectionFactoryInterface;
use PhpExtended\Score\ScoreFactoryInterface;
use PhpExtended\Score\ScorePolicyFactoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * ScoreCalculationDefinitionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Merge\ScoreCalculationDefinition
 *
 * @internal
 *
 * @small
 */
class ScoreCalculationDefinitionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScoreCalculationDefinition
	 */
	protected ScoreCalculationDefinition $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScoreCalculationDefinition(
			'source',
			'challenger',
			$this->getMockForAbstractClass(ScoreCollectionFactoryInterface::class),
			$this->getMockForAbstractClass(ScoreFactoryInterface::class),
			$this->getMockForAbstractClass(ScorePolicyFactoryInterface::class),
		);
	}
	
}
