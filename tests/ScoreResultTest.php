<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Merge\ScoreResult;
use PhpExtended\Score\ScoreInterface;
use PHPUnit\Framework\TestCase;

/**
 * ScoreResultTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Merge\ScoreResult
 *
 * @internal
 *
 * @small
 */
class ScoreResultTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScoreResult
	 */
	protected ScoreResult $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScoreResult(
			'namespace',
			'classname',
			'fieldname',
			$this->getMockForAbstractClass(ScoreInterface::class),
		);
	}
	
}
