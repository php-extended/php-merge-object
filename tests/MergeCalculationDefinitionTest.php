<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Merge\MergeCalculationDefinition;
use PhpExtended\Vote\BiasFactoryInterface;
use PhpExtended\Vote\CandidateFactoryInterface;
use PhpExtended\Vote\CitizenFactoryInterface;
use PhpExtended\Vote\ElectionRunnerFactoryInterface;
use PhpExtended\Vote\VotingMethodFactoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * MergeCalculationDefinitionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Merge\MergeCalculationDefinition
 *
 * @internal
 *
 * @small
 */
class MergeCalculationDefinitionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MergeCalculationDefinition
	 */
	protected MergeCalculationDefinition $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MergeCalculationDefinition(
			'source',
			'challenger',
			$this->getMockForAbstractClass(VotingMethodFactoryInterface::class),
			$this->getMockForAbstractClass(CitizenFactoryInterface::class),
			$this->getMockForAbstractClass(CandidateFactoryInterface::class),
			$this->getMockForAbstractClass(BiasFactoryInterface::class),
			$this->getMockForAbstractClass(ElectionRunnerFactoryInterface::class),
		);
	}
	
}
