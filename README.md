# php-extended/php-merge-object
An implementation of the php-merge-interface library.

![coverage](https://gitlab.com/php-extended/php-merge-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-merge-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-merge-object ^8`


## Basic Usage

This library may be used the following way :

- First Step : to calculate the score of any data provider (such score may be
interpreted as the accuracy of that data provider to be accurate for a given
type of data) :

```php

use PhpExtended\Merge\Scorer;
use PhpExtended\Merge\ScoreCalculationDefinitionList;

/* @var $source \PhpExtended\Record\RecordProviderInterface */ 
/* @var $srcNamespace string */
/* @var $srcClassname string */
/* @var $challenger \PhpExtended\Record\AssignableRecordProviderInterface */
/* @var $chlNamespace string */
/* @var $chlClassname string */

/* @var $scoreCollectionFactory \PhpExtended\Score\ScoreCollectionFactoryInterface */
/* @var $scoreFactory \PhpExtended\Score\ScoreFactory */
/* @var $scorePolicyFactory \PhpExtended\Score\ScorePolicyFactoryInterface */

$scoreCalculationDefinitionList = new ScoreCalculationDefinitionList();
foreach(['srcAttr1' => 'dstAttr1', 'srcAttr2' => 'dstAttr2'] as $srcAttr => $dstAttr)
{
	$scoreCalculationDefinition = new ScoreCalculationDefinition(
		$srcAttr,
		$dstAttr,
		$scoreCollectionFactory,
		$scoreFactory,	/* the score factory may differ for each field name, but not needed every time */
		$scorePolicyFactory
	);
	$scoreCalculationDefinitionList->append($scoreCalculationDefinition);
}

$scorer = new Scorer();

$results = $scorer->calculateScore($source, $srcNamespace, $srcClassname, $challenger, $chlNamespace, $chlClassname, $scoreCaculationDefinitionList);

foreach($results as $result)
{
	/* @var $result \PhpExtended\Merge\ScoreResultInterface */
	// get namespace, classname, fieldname and score
	// for each $chlNamespace, $chlClassname and $dstAttr
}

```


- Second Step : to assign records of a given data provider (record by record)
to another source record provider. The best fit is given by the comparator
which should give a perfect ordering of the records by measuring the distance
between two records.

```php

use PhpExtended\Merge\Assigner;

/* @var $source \PhpExtended\Record\RecordProviderInterface */
/* @var $srcNamespace string */
/* @var $srcClassname string */
/* @var $challenger \PhpExtended\Record\AssignableRecordProviderInterface */
/* @var $chlNamespace string */
/* @var $chlClassname string */
/* @var $comparator \PhpExtended\Record\CompatorInterface */

$assigner = new Assigner();
$assignments = $assigner->doAssignments($source, $srcNamespace, $srcClassname, $challenger, $chlNamespace, $chlClassname, $comparator);

// returns the number of records that were effectively assigned

```


- Third Step : merges the assigned record values into the main record, by giving
individual informations to change to the main record.
Note that this method will not create non-existent records only from the 
challenger sources, it must be created by another procedure.

```php

use PhpExtended\Merge\Merger;

/* @var $source \PhpExtended\Record\RecordProviderInterface */
/* @var $srcNamespace string */
/* @var $srcClassname string */
/* @var $challengerList \PhpExtended\Record\AssignableRecordProviderListInterface */
// note that the challenger list must be created with all the available challengers
// to avoid at most the unsolvable elections
/* @var $chlNamespace string */
/* @var $chlClassname string */

// warning : the challengers should be normalized (via an interface if possible)
/* @var $votingMethodFactory \PhpExtended\Vote\VotingMethodFactoryInterface */
/* @var $citizenFactory \PhpExtended\Vote\CitizenFactoryInterface */
/* @var $candidateFactory \PhpExtended\Vote\CandidateFactoryInterface */
/* @var $argumentFactory \PhpExtended\Vote\ArgumentFactoryInterface */
/* @var $biasFactory \PhpExtended\Vote\BiasFactoryInterface */
/* @var $electionRunnerFactory \PhpExtended\Vote\ElectionRunnerFactoryInterface */

$mergeCalculationDefintitionList = new MergeCalculationDefinitionList();
foreach(['srcAttr1' => 'dstAttr1', 'srcAttr2' => 'dstAttr2'] as $srcAttr => $dstAttr)
{
	$mergeCalculationDefinition = new MergeCalculationDefinition(
		$srcAttr,
		$dstAttr,	// warning, should be the same for each challenger in the list
		$votingMethodFactory,
		$citizenFactory, // warning, should be the same for each challenger in the list
		$candidateFactory,
		$argumentFactory,
		$biasFactory, // warning, should be the same for each challenger in the list
		$electionRunnerFactory
	);
	$mergeCalculationDefinitionList->append($mergeCalculationDefinition);
}

$merger = new Merger();
$informations = $merger->doMerge($source, $srcNamespace, $srcClassname, $challengerList, $chlNamespace, $chlClassname, $mergeCalculationDefinitionList);

foreach($informations as $information)
{
	/* @var $information \PhpExtended\Information\InformationInterface */
	// get the $srcNamespace, $srcClassname, $sourceId and $srcFieldName
	// alongside with the $value
}
```


## License

MIT (See [license file](LICENSE)).
