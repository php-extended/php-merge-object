<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Vote\BiasFactoryInterface;
use PhpExtended\Vote\CandidateFactoryInterface;
use PhpExtended\Vote\CitizenFactoryInterface;
use PhpExtended\Vote\ElectionRunnerFactoryInterface;
use PhpExtended\Vote\VotingMethodFactoryInterface;

/**
 * MergeCalculationDefinition class file.
 * 
 * This class is a simple implementation of the MergeCalculationDefinitionInterface.
 * 
 * @author Anastaszor
 */
class MergeCalculationDefinition implements MergeCalculationDefinitionInterface
{
	
	/**
	 * The source field name.
	 * 
	 * @var string
	 */
	protected string $_srcFieldName;
	
	/**
	 * The challenger field name.
	 * 
	 * @var string
	 */
	protected string $_challFieldName;
	
	/**
	 * The voting method factory.
	 * 
	 * @var VotingMethodFactoryInterface
	 */
	protected VotingMethodFactoryInterface $_votingMethodFactory;
	
	/**
	 * The citizen factory.
	 * 
	 * @var CitizenFactoryInterface
	 */
	protected CitizenFactoryInterface $_citizenFactory;
	
	/**
	 * The candidate factory interface.
	 * 
	 * @var CandidateFactoryInterface
	 */
	protected CandidateFactoryInterface $_candidateFactory;
	
	/**
	 * The bias factory.
	 * 
	 * @var BiasFactoryInterface
	 */
	protected BiasFactoryInterface $_biasFactory;
	
	/**
	 * The election runner factory.
	 * 
	 * @var ElectionRunnerFactoryInterface
	 */
	protected ElectionRunnerFactoryInterface $_elecRunnerFactory;
	
	/**
	 * Builds a new MergeCalculationDefinition with the needed objects.
	 * 
	 * @param string $srcFieldName
	 * @param string $challFieldName
	 * @param VotingMethodFactoryInterface $votingMethodFactory
	 * @param CitizenFactoryInterface $citizenFactory
	 * @param CandidateFactoryInterface $candidateFactory
	 * @param BiasFactoryInterface $biasFactory
	 * @param ElectionRunnerFactoryInterface $elecRunnerFactory
	 */
	public function __construct(
		string $srcFieldName,
		string $challFieldName,
		VotingMethodFactoryInterface $votingMethodFactory,
		CitizenFactoryInterface $citizenFactory,
		CandidateFactoryInterface $candidateFactory,
		BiasFactoryInterface $biasFactory,
		ElectionRunnerFactoryInterface $elecRunnerFactory
	) {
		$this->_srcFieldName = $srcFieldName;
		$this->_challFieldName = $challFieldName;
		$this->_votingMethodFactory = $votingMethodFactory;
		$this->_citizenFactory = $citizenFactory;
		$this->_candidateFactory = $candidateFactory;
		$this->_biasFactory = $biasFactory;
		$this->_elecRunnerFactory = $elecRunnerFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getSourceFieldName()
	 */
	public function getSourceFieldName() : string
	{
		return $this->_srcFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getChallengerFieldName()
	 */
	public function getChallengerFieldName() : string
	{
		return $this->_challFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getVotingMethodFactory()
	 */
	public function getVotingMethodFactory() : VotingMethodFactoryInterface
	{
		return $this->_votingMethodFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getCitizenFactory()
	 */
	public function getCitizenFactory() : CitizenFactoryInterface
	{
		return $this->_citizenFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getCandidateFactory()
	 */
	public function getCandidateFactory() : CandidateFactoryInterface
	{
		return $this->_candidateFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getBiasFactory()
	 */
	public function getBiasFactory() : BiasFactoryInterface
	{
		return $this->_biasFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergeCalculationDefinitionInterface::getElectionRunnerFactory()
	 */
	public function getElectionRunnerFactory() : ElectionRunnerFactoryInterface
	{
		return $this->_elecRunnerFactory;
	}
	
}
