<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Score\ScoreCollectionFactoryInterface;
use PhpExtended\Score\ScoreFactoryInterface;
use PhpExtended\Score\ScorePolicyFactoryInterface;

/**
 * ScoreCalculationDefinition class file.
 * 
 * This class is a simple implementation of the ScoreCalculationDefinitionInterface.
 * 
 * @author Anastaszor
 */
class ScoreCalculationDefinition implements ScoreCalculationDefinitionInterface
{
	
	/**
	 * The source field name.
	 * 
	 * @var string
	 */
	protected string $_srcFieldName;
	
	/**
	 * The challenger field name.
	 * 
	 * @var string
	 */
	protected string $_challFieldName;
	
	/**
	 * The score collection factory.
	 * 
	 * @var ScoreCollectionFactoryInterface
	 */
	protected ScoreCollectionFactoryInterface $_scoreCollecFactory;
	
	/**
	 * The score factory.
	 * 
	 * @var ScoreFactoryInterface
	 */
	protected ScoreFactoryInterface $_scoreFactory;
	
	/**
	 * The score policy factory.
	 * 
	 * @var ScorePolicyFactoryInterface
	 */
	protected ScorePolicyFactoryInterface $_scorePolicyFactory;
	
	/**
	 * Builds a new ScoreCalculationDefinition with the given arguments.
	 * 
	 * @param string $sourceFieldName
	 * @param string $challengerFieldName
	 * @param ScoreCollectionFactoryInterface $scoreCollecFactory
	 * @param ScoreFactoryInterface $scoreFactory
	 * @param ScorePolicyFactoryInterface $scorePolicyFactory
	 */
	public function __construct(
		string $sourceFieldName,
		string $challengerFieldName,
		ScoreCollectionFactoryInterface $scoreCollecFactory,
		ScoreFactoryInterface $scoreFactory,
		ScorePolicyFactoryInterface $scorePolicyFactory
	) {
		$this->_srcFieldName = $sourceFieldName;
		$this->_challFieldName = $challengerFieldName;
		$this->_scoreCollecFactory = $scoreCollecFactory;
		$this->_scoreFactory = $scoreFactory;
		$this->_scorePolicyFactory = $scorePolicyFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreCalculationDefinitionInterface::getSourceFieldname()
	 */
	public function getSourceFieldname() : string
	{
		return $this->_srcFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreCalculationDefinitionInterface::getChallengerFieldname()
	 */
	public function getChallengerFieldname() : string
	{
		return $this->_challFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreCalculationDefinitionInterface::getScoreCollectionFactory()
	 */
	public function getScoreCollectionFactory() : ScoreCollectionFactoryInterface
	{
		return $this->_scoreCollecFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreCalculationDefinitionInterface::getScoreFactory()
	 */
	public function getScoreFactory() : ScoreFactoryInterface
	{
		return $this->_scoreFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreCalculationDefinitionInterface::getScorePolicyFactory()
	 */
	public function getScorePolicyFactory() : ScorePolicyFactoryInterface
	{
		return $this->_scorePolicyFactory;
	}
	
}
