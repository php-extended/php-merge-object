<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use InvalidArgumentException;
use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObject;
use PhpExtended\Record\RecordInterface;
use PhpExtended\Record\RecordProviderInterface;
use PhpExtended\Vote\BiasFactoryInterface;
use PhpExtended\Vote\BiasInterface;
use PhpExtended\Vote\CandidateFactoryInterface;
use PhpExtended\Vote\CitizenFactoryInterface;
use PhpExtended\Vote\CitizenInterface;
use PhpExtended\Vote\ElectionRunnerFactoryInterface;
use PhpExtended\Vote\InvalidCandidateThrowable;
use PhpExtended\Vote\InvalidVoteThrowable;
use PhpExtended\Vote\UnsolvableSituationThrowable;
use PhpExtended\Vote\VotingMethodFactoryInterface;

/**
 * Merger class file.
 * 
 * This class is a simple implementation of the MergerInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class Merger implements MergerInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\MergerInterface::doMerge()
	 * @throws InvalidArgumentException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function doMerge(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		array $challengerList,
		string $chlNamespace,
		string $chlClassname,
		array $calcDefList
	) : array {
		/** @var array<integer, InformationInterface> $informations */
		$informations = [];
		
		/** @var RecordInterface $sourceRecord */
		foreach($source->getAllRecords($srcNamespace, $srcClassname) as $k => $sourceRecord)
		{
			foreach($calcDefList as $mergeCalcDef)
			{
				/** @var CitizenFactoryInterface $citizenFactory */
				$citizenFactory = $mergeCalcDef->getCitizenFactory();
				/** @var CandidateFactoryInterface $candidateFactory */
				$candidateFactory = $mergeCalcDef->getCandidateFactory();
				$citizens = [];
				/** @var null|boolean|integer|float|string $value */
				$value = null;
				
				foreach($challengerList as $challengerSource)
				{
					$challengerObjects = $challengerSource->getAssignedRecords($chlNamespace, $chlClassname, $sourceRecord->getIdentifier());
					
					foreach($challengerObjects as $challengerObject)
					{
						$id = $chlNamespace.'_'.$chlClassname.'-'.((string) $k).'-'.$challengerObject->getIdentifier();
						/** @var null|boolean|integer|float|string $value */
						$value = $challengerObject->getValue($mergeCalcDef->getChallengerFieldName());
						
						switch(\gettype($value))
						{
							case 'string':
								$candidate = $candidateFactory->createStringCandidate($id, $value);
								break;
								
							case 'double':
								$candidate = $candidateFactory->createFloatCandidate($id, $value);
								break;
								
							case 'integer':
								$candidate = $candidateFactory->createIntegerCandidate($id, $value);
								break;
								
							case 'boolean':
							default:
								$candidate = $candidateFactory->createBooleanCandidate($id, $value);
						}
						
						/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
						$citizens[] = $citizenFactory->createCitizen($id, [$candidate]);
					}
				}
				
				$elecRunnerFactory = $mergeCalcDef->getElectionRunnerFactory();
				$votingMethodFactory = $mergeCalcDef->getVotingMethodFactory();
				/** @var BiasFactoryInterface $biasFactory */
				$biasFactory = $mergeCalcDef->getBiasFactory();
				
				switch(\gettype($value))
				{
					case 'string':
						$bias = $biasFactory->createStringBias();
						break;
					
					case 'double':
						$bias = $biasFactory->createFloatBias();
						break;
					
					case 'integer':
						$bias = $biasFactory->createIntegerBias();
						break;
					
					case 'boolean':
					default:
						$bias = $biasFactory->createBooleanBias();
				}
				$srcFieldName = $mergeCalcDef->getSourceFieldName();
				$default = $sourceRecord->getValue($srcFieldName);
				/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
				$value = (string) $this->runElection($elecRunnerFactory, $votingMethodFactory, $bias, $citizens, $default);
				$id = $sourceRecord->getIdentifier().'-'.$mergeCalcDef->getSourceFieldName();
				$information = new InformationObject($id, \get_class($sourceRecord));
				$information->addKey($sourceRecord->getIdentifier(), $sourceRecord->getIdentifier());
				$information->addData($srcFieldName, $value);
				$informations[] = $information;
			}
		}
		
		return $informations;
	}
	
	/**
	 * Runs the election and gets the first result.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param ElectionRunnerFactoryInterface $electRunnerFactory
	 * @param VotingMethodFactoryInterface $votingMethodFactory
	 * @param BiasInterface<T> $bias
	 * @param array<integer, CitizenInterface<T>> $citizens
	 * @param ?T $default
	 * @return ?T
	 * @throws InvalidCandidateThrowable if one of the candidates is invalid
	 * @throws InvalidVoteThrowable if one of the votes is invalid
	 * @throws UnsolvableSituationThrowable if the vote cannot be resolved
	 * @psalm-suppress MissingThrowsDocblock
	 */
	protected function runElection(
		ElectionRunnerFactoryInterface $electRunnerFactory,
		VotingMethodFactoryInterface $votingMethodFactory,
		BiasInterface $bias,
		array $citizens,
		$default
	) {
		$runner = $electRunnerFactory->createElectionRunner();
		$votingMethod = $votingMethodFactory->createVotingMethod();
		$result = $runner->runElection($votingMethod, [$bias], $citizens);
		
		foreach($result->getRankings() as $candidateResult)
		{
			return $candidateResult->getCandidate()->getValue();
		}
		
		return $default;
	}
	
}
