<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Score\ScoreInterface;

/**
 * ScoreResult class file.
 * 
 * This class is a simple implementation of the ScoreResultInterface.
 * 
 * @author Anastaszor
 */
class ScoreResult implements ScoreResultInterface
{
	
	/**
	 * The namespace.
	 * 
	 * @var string
	 */
	protected string $_namespace;
	
	/**
	 * The class name.
	 * 
	 * @var string
	 */
	protected string $_classname;
	
	/**
	 * The field name.
	 * 
	 * @var string
	 */
	protected string $_fieldname;
	
	/**
	 * The score.
	 * 
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_score;
	
	/**
	 * Builds a new ScoreResult with the given arguments.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $fieldname
	 * @param ScoreInterface $score
	 */
	public function __construct(string $namespace, string $classname, string $fieldname, ScoreInterface $score)
	{
		$this->_namespace = $namespace;
		$this->_classname = $classname;
		$this->_fieldname = $fieldname;
		$this->_score = $score;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreResultInterface::getNamespace()
	 */
	public function getNamespace() : string
	{
		return $this->_namespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreResultInterface::getClassname()
	 */
	public function getClassname() : string
	{
		return $this->_classname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreResultInterface::getFieldname()
	 */
	public function getFieldname() : string
	{
		return $this->_fieldname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScoreResultInterface::getScore()
	 */
	public function getScore() : ScoreInterface
	{
		return $this->_score;
	}
	
}
