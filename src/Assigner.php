<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Record\AssignableRecordProviderInterface;
use PhpExtended\Record\RecordComparatorInterface;
use PhpExtended\Record\RecordProviderInterface;

/**
 * Assigner class file.
 * 
 * This class is a simple implementation of the AssignerInterface.
 * 
 * @author Anastaszor
 */
class Assigner implements AssignerInterface
{
	
	/**
	 * The maximum value of the distance between two objects to assign them.
	 * 
	 * @var float
	 */
	protected float $_threshold = 0.2;
	
	/**
	 * Builds a new Assigner with the given threshold for the maximal distance
	 * between two objects to assign them.
	 * 
	 * @param float $threshold
	 */
	public function __construct(float $threshold = 0.2)
	{
		$this->_threshold = $threshold;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\AssignerInterface::doAssignments()
	 */
	public function doAssignments(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		AssignableRecordProviderInterface $challenger,
		string $chlNamespace,
		string $chlClassname,
		RecordComparatorInterface $comparator
	) : int {
		$count = 0;
		
		foreach($challenger->getAllUnassignedRecords($chlNamespace, $chlClassname) as $challRecord)
		{
			$sourceRecords = $source->getAllRecords($srcNamespace, $srcClassname);
			$found = null;
			$minScore = \PHP_INT_MAX;
			
			foreach($sourceRecords as $sourceRecord)
			{
				$score = \abs($comparator->compare($sourceRecord, $challRecord));
				if($score < $minScore)
				{
					$minScore = $score;
					$found = $sourceRecord;
				}
			}
			if(null !== $found && $minScore < $this->_threshold)
			{
				if($challRecord->assign($found->getIdentifier()))
				{
					$count++;
				}
			}
		}
		
		return $count;
	}
	
}
