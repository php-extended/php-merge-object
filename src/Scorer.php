<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use InvalidArgumentException;
use PhpExtended\Record\AssignableRecordInterface;
use PhpExtended\Record\AssignableRecordProviderInterface;
use PhpExtended\Record\RecordProviderInterface;

/**
 * Scorer class file.
 * 
 * This class is a simple implementation of the ScorerInterface.
 * 
 * @author Anastaszor
 */
class Scorer implements ScorerInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Merge\ScorerInterface::calculateScore()
	 * @throws InvalidArgumentException
	 */
	public function calculateScore(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		AssignableRecordProviderInterface $challenger,
		string $chlNamespace,
		string $chlClassname,
		array $calcDefList
	) : array {
		$scoreResultList = [];
		
		foreach($calcDefList as $scoreCalcDef)
		{
			/** @var ScoreCalculationDefinition $scoreCalcDef */
			$scoreCollFactory = $scoreCalcDef->getScoreCollectionFactory();
			$scoreCollection = $scoreCollFactory->createScoreCollection();
			$scoreFactory = $scoreCalcDef->getScoreFactory();
			$sourceAttribute = $scoreCalcDef->getSourceFieldname();
			$targetAttribute = $scoreCalcDef->getChallengerFieldname();
			
			foreach($challenger->getAllRecords($chlNamespace, $chlClassname) as $challRecord)
			{
				if($challRecord instanceof AssignableRecordInterface)
				{
					$sourceRecord = $source->getRecord($srcNamespace, $srcClassname, $challRecord->getAssignmentId());
					$scoreCollection->add($scoreFactory->createScore([
						$sourceRecord->getValue($sourceAttribute),
						$challRecord->getValue($targetAttribute),
					]));
				}
			}
			$policyFactory = $scoreCalcDef->getScorePolicyFactory();
			$policy = $policyFactory->createScorePolicy();
			$score = $scoreCollection->resolve($policy);
			$scoreResultList[] = new ScoreResult($chlNamespace, $chlClassname, $targetAttribute, $score);
		}
		
		return $scoreResultList;
	}
	
}
